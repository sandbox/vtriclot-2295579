<?php
/**
 * @file
 * Unapproved Comments Delete Admin form.
 */

/**
 * Implements hook_form().
 */
function unapproved_comments_delete_admin_form($form, &$form_state) {
  // Define a global variable to determine if we are in the confirmation form
  // to display or hide the help text according.
  global $_unapproved_comments_delete_confirm_form;

  if (!isset($form_state['storage']['confirm'])) {
    $_unapproved_comments_delete_confirm_form = FALSE;

    $days = variable_get('unapproved_comments_delete_days', array('default' => 0));

    $form['settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Configuration'),
    );

    $form['settings']['unapproved_comments_delete_track_default'] = array(
      '#type' => 'checkbox',
      '#title' => t('Track unapproved comments for all content types'),
      '#description' => t('All content types with comment enabled will be tracked. If no specific number of days is set for each content type, it will use the default value.'),
      '#default_value' => (boolean) $days['default'],
    );
    $form['settings']['unapproved_comments_delete_days_default'] = array(
      '#type' => 'textfield',
      '#title' => t('Maximum number of days to keep unapproved comments'),
      '#description' => t('Unapproved comments will be delete after this period if the related content type is tracked. This value can be overridden on a content type basis.') ,
      '#default_value' => (!empty($days['default'])) ? $days['default'] : UNAPPROVED_COMMENTS_DELETE_DAYS_DEFAULT,
      '#element_validate' => array('element_validate_integer_positive'),
      '#states' => array(
        'visible' => array(
          ':input[name=unapproved_comments_delete_track_default]' => array('checked' => TRUE),
        ),
        'required' => array(
          ':input[name=unapproved_comments_delete_track_default]' => array('checked' => TRUE),
        ),
      ),
    );

    $rows = array();
    foreach (node_type_get_names() as $key => $name) {
      if ((((boolean) $days['default']) && in_array(variable_get('comment_' . $key), array(COMMENT_NODE_HIDDEN, COMMENT_NODE_OPEN))) || !empty($days[$key])) {
        $days_type = (!empty($days[$key])) ? $days[$key] : $days['default'];
        $rows[] = array(
          $name,
          $key,
          $days_type . format_plural($days_type, ' day', ' days'),
          l(t('Edit settings'), 'admin/structure/types/manage/' . $key, array(
            'fragment' => 'edit-comment',
            'query' => array(
              'destination' => 'admin/config/content/unapproved_comments_delete'
            ),
            'attributes' => array(
              'title' => t('Edit the number of days for the @bundle.', array('@bundle' => $key)),
            ),
          )),
        );
      }
    }

    if (!empty($rows)) {
      $table_header = array(
        'Content Type',
        'Machine Name',
        'Delete unapproved comment after',
        'Action',
      );
      $form['settings']['unapproved_comments_delete_overview'] = array(
        '#prefix' => '<strong>' . t('Currently tracked content types') . '</strong>',
        '#markup' => theme('table', array('header' => $table_header, 'rows' => $rows)),
      );
    }

    $form['batch'] = array(
      '#type' => 'fieldset',
      '#title' => t('Bulk operation'),
    );

    $form['batch']['description'] = array(
      '#markup' => '<p>' . t("Running the bulk operation will delete all eligible comments based on the configuration above. Useful to do a first cleaning when you enable the module or if you don't want to use the cron.") . '</p>',
    );

    $form['batch']['run'] = array(
      '#type' => 'submit',
      '#value' => t('Run process'),
      '#submit' => array('unapproved_comments_delete_launch_batch'),
    );

    $form['cron'] = array(
      '#type' => 'fieldset',
      '#title' => t('Cron'),
    );

    $form['cron']['unapproved_comments_delete_cron_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Delete unapproved comments during cron'),
      '#description' => t('If this option is not enabled, the only ways to delete unapproved comments are bulk operation or drush command both launched manually.'),
      '#default_value' => variable_get('unapproved_comments_delete_cron_enabled', UNAPPROVED_COMMENTS_DELETE_CRON_ENABLED),
    );

    $form['cron']['unapproved_comments_delete_cron_limited'] = array(
      '#type' => 'checkbox',
      '#title' => t('Limit the number of unapproved comments deleted by cron'),
      '#default_value' => (boolean) variable_get('unapproved_comments_delete_limit_per_cron', UNAPPROVED_COMMENTS_DELETE_LIMIT_PER_PROCESS),
      '#states' => array(
        'visible' => array(
          ':input[name=unapproved_comments_delete_cron_enabled]' => array('checked' => TRUE),
        ),
      ),
    );

    $limit = variable_get('unapproved_comments_delete_limit_per_cron', UNAPPROVED_COMMENTS_DELETE_LIMIT_PER_PROCESS);
    $form['cron']['unapproved_comments_delete_limit_per_cron'] = array(
      '#type' => 'textfield',
      '#title' => t('How many unapproved comments do you want to delete per cron run?'),
      '#description' => t('This option defines the maximum amount of unapproved comments to delete per cron run. A too small number may not be enough to absorb the amount of unapproved comments. A too big number may run the cron too long. As there is kind of random selection when a limit is set, if a comment is selected to be deleted and if this comment has replies, it will also be deleted.'),
      '#element_validate' => array('element_validate_integer'),
      '#default_value' => ($limit) ? $limit : '',
      '#states' => array(
        'visible' => array(
          ':input[name=unapproved_comments_delete_cron_enabled]' => array('checked' => TRUE),
          ':input[name=unapproved_comments_delete_cron_limited]' => array('checked' => TRUE),
        ),
      ),
    );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
    );

    return $form;
  }
  else {
    // Hide the help text on the confirmation form.
    $_unapproved_comments_delete_confirm_form = TRUE;

    // Store data to pass them through the confirmation form.
    $form['unapproved_comments_delete_track_default'] = array(
      '#type' => 'value',
      '#value' => $form_state['values']['unapproved_comments_delete_track_default'],
    );
    $form['unapproved_comments_delete_days_default'] = array(
      '#type' => 'value',
      '#value' => $form_state['values']['unapproved_comments_delete_days_default'],
    );
    $form['unapproved_comments_delete_cron_enabled'] = array(
      '#type' => 'value',
      '#value' => $form_state['values']['unapproved_comments_delete_cron_enabled'],
    );
    $form['unapproved_comments_delete_cron_limited'] = array(
      '#type' => 'value',
      '#value' => $form_state['values']['unapproved_comments_delete_cron_limited'],
    );
    $form['unapproved_comments_delete_limit_per_cron'] = array(
      '#type' => 'value',
      '#value' => $form_state['values']['unapproved_comments_delete_limit_per_cron'],
    );

    // Load content types with custom settings defined to list them.
    $days = variable_get('unapproved_comments_delete_days', array('default' => 0));
    unset($days['default']);

    // Display the confirmation form.
    return confirm_form(
      $form,
      t('Are you sure you want to stop tracking all content types?'),
      'admin/config/content/unapproved_comments_delete',
      t('Only content types tracked with custom settings will remain tracked. (ie. %bundles)', array('%bundles' => implode(', ', array_keys($days))))
    );
  }
}

/**
 * Form validate handler for the admin settings form.
 *
 * We are not validating anything here. We just choose to display or hide
 * the confirmation form. This step is displayed only if we choose to not track
 * all content types anymore.
 */
function unapproved_comments_delete_admin_form_validate($form, &$form_state) {
  $days = variable_get('unapproved_comments_delete_days', array('default' => 0));

  if (!isset($form_state['storage']['confirm']) &&
    ((((boolean) $days['default']) == $form_state['values']['unapproved_comments_delete_track_default']) || ($days['default'] == 0))) {
    $form_state['storage']['confirm'] = TRUE;
  }
}

/**
 * Form submit handler for the admin settings form.
 */
function unapproved_comments_delete_admin_form_submit($form, &$form_state) {
  // The confirmation form hasn't been passed yet, rebuild the form.
  if (!isset($form_state['storage']['confirm'])) {
    $form_state['storage']['values'] = $form_state['values'];
    $form_state['storage']['confirm'] = TRUE;
    $form_state['rebuild'] = TRUE;
  }
  // Confirmation step has been passed, save data.
  else {
    $days = variable_get('unapproved_comments_delete_days', array('default' => 0));

    // Update default settings.
    $days['default'] = (!empty($form_state['values']['unapproved_comments_delete_track_default'])) ? $form_state['values']['unapproved_comments_delete_days_default'] : 0;

    // Find all contents which was using a custom settings = new default one.
    foreach ($days as $key => $value) {
      if ($key != 'default' && $value == $days['default']) {
        unset($days[$key]);
      }
    }

    // Calculate the limit based on checkbox and value filled.
    $limit = 0;
    if ($form_state['values']['unapproved_comments_delete_cron_limited'] && !empty($form_state['values']['unapproved_comments_delete_limit_per_cron'])) {
      $limit = $form_state['values']['unapproved_comments_delete_limit_per_cron'];
    }

    // Save variables.
    variable_set('unapproved_comments_delete_days', $days);
    variable_set('unapproved_comments_delete_cron_enabled', $form_state['values']['unapproved_comments_delete_cron_enabled']);
    variable_set('unapproved_comments_delete_limit_per_cron', $limit);


    drupal_set_message(t('The configuration options have been saved.'));
  }
}
