<?php
/**
 * @file
 * Drush integration for Unapproved Comments Delete module.
 */

/**
 * Implements hook_drush_command().
 */
function unapproved_comments_delete_drush_command() {
  $items = array();

  $items['unapproved-comments-delete'] = array(
    'description' => 'Deletes unapproved comments based on the currently saved settings at admin/config/content/unapproved_comments_delete.',
    'options' => array(
      'cids' => 'A comma delimited list of numeric comment-ids to check and delete.',
      'nids' => 'A comma delimited list of numeric node-ids to find related comments to check and delete.',
      'bundles' => 'A comma delimited list of content type to find related comments to check and delete.',
      'limit' => 'Limit the number of comments deleted by this process.',
    ),
    'examples' => array(
      'drush ucd --cids=3689,1964 --nids=792,619 --bundles=article --limit=100' => 'Will check comments 3689 & 1964, comments related to nodes 792 & 619, ' .
      'comments related to nodes of type article and delete them if matching requirements from configuration. It will limit the deletion to 100 comments.' .
      'If it\'s called without any option, it will find and delete comments based on configuration from admin/config/content/unapproved_comments_delete.',
    ),
    'aliases' => array('ucd'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  return $items;
}

/**
 * A command callback.
 */
function drush_unapproved_comments_delete() {
  $limit = drush_get_option('limit', 0);
  if (!is_numeric($limit) || $limit < 0) {
    return drush_set_error('UNAPPROVED_COMMENTS_DELETE_WRONG_LIMIT', dt('Limit option must be an integer.'));
  }

  $count = 0;

  $cids = drush_get_option('cids');
  if (!empty($cids) && $cids = array_filter(explode(',', $cids), 'is_numeric')) {
    $cids_to_delete = _unapproved_comments_delete_select_cids(array('cids' => $cids), $limit);

    if (!empty($cids_to_delete)) {
      comment_delete_multiple($cids_to_delete);
      $count += count($cids_to_delete);
      drush_log(t('@count_delete comments (and replies) deleted from cids list.', array('@count_delete' => count($cids_to_delete))), 'success');
    }
  }

  $nids = drush_get_option('nids');
  if ((!$limit || $count < $limit) && !empty($nids) && $nids = array_filter(explode(',', $nids), 'is_numeric')) {
    $cids_to_delete = _unapproved_comments_delete_select_cids(array('nids' => $nids), $limit-$count);

    if (!empty($cids_to_delete)) {
      comment_delete_multiple($cids_to_delete);
      $count += count($cids_to_delete);
      drush_log(t('@count_delete comments (and replies) deleted from nids list.', array('@count_delete' => count($cids_to_delete))), 'success');
    }
  }

  $bundles = drush_get_option('bundles');
  if ((!$limit || $count < $limit) && !empty($bundles) && $bundles = explode(',', $bundles)) {
    $cids_to_delete = _unapproved_comments_delete_select_cids(array('bundles' => $bundles), $limit-$count);

    if (!empty($cids_to_delete)) {
      comment_delete_multiple($cids_to_delete);
      drush_log(t('@count_delete comments (and replies) deleted from bundles list.', array('@count_delete' => count($cids_to_delete))), 'success');
    }
  }

  // We haven't specify anything, we want to do a full clean.
  if (empty($cids) && empty($nids) && empty($bundles)) {
    $cids_to_delete = _unapproved_comments_delete_select_cids(array(), $limit);

    if (!empty($cids_to_delete)) {
      comment_delete_multiple($cids_to_delete);
      drush_log(t('@count_delete comments (and replies) deleted.', array('@count_delete' => count($cids_to_delete))), 'success');
    }
  }
}
